﻿using AdminPanel.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AdminPanel.ViewModel
{
    class MainViewModel : Bindable
    {

        


        public ICommand SelectedCustomerEdit => new Command(async (Object obj) =>
        {
            Console.WriteLine("ITEM SELECTED");
        });

        private ObservableCollection<object> viewModelList;
        public ObservableCollection<object> ViewModelList { get { return viewModelList; } }

        public MainViewModel()
        {
            viewModelList = new ObservableCollection<object>();
            viewModelList.Add(new NewCustomerViewModel());
            viewModelList.Add(new EditCustomerViewModel());
            viewModelList.Add(new DeleteCustomerViewModel());
            viewModelList.Add(new NewLoginViewModel());
        }


    

    }
}
