﻿using AdminPanel.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminPanel.ViewModel
{
    class NewLoginViewModel : Bindable
    {

        private ObservableCollection<Customer> customerList;
        public ObservableCollection<Customer> CustomerList { get { return customerList; } }

        private ObservableCollection<Role> roleList;
        public ObservableCollection<Role> RoleList { get { return roleList; } }

        private Customer selectedCustomer = new Customer();
        public Customer SelectedCustomer { get { return selectedCustomer; } set { selectedCustomer = value; OnPropertyChanged(); } }

        private Role selectedRole = new Role();
        public Role SelectedRole { get { return selectedRole; } set { selectedRole = value; OnPropertyChanged(); } }

        private User user = new User();
        public User User { get { return user; } set { user = value; OnPropertyChanged(); } }

        public NewLoginViewModel()
        {
            Console.WriteLine("TAB ÅBNET");
            customerList = new ObservableCollection<Customer>();
            roleList = new ObservableCollection<Role>();
            GetCustomersAsync();
            GetRolesAsync();
        }

        private async Task GetCustomersAsync()
        {
            var response = await APIConnection.Get("api/customers/");
            var customers = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            foreach (var customer in customers)
            {
                customerList.Add(customer);
            }
        }
        private async Task GetRolesAsync()
        {
            var response = await APIConnection.Get("api/roles/");
            var roles = response.Content.ReadAsAsync<IEnumerable<Role>>().Result;
            foreach (var role in roles)
            {
                roleList.Add(role);
            }
        }

        private async Task<bool> CheckUsername()
        {
            var response = await APIConnection.Get($"api/getusernames/{User.Username}");
            User checkUser = new User();
            checkUser = await response.Content.ReadAsAsync<User>();


            //If username exist, return true
            if (checkUser == null)
            {
                return false;
            }
            else if (checkUser.Username.ToLower().Equals(User.Username.ToLower()))
            {
                return true;
            } else
            {
                return false;
            }
        }

        public ICommand NewLoginCommand => new Command(async (object obj) =>
        {
            if (await CheckUsername() == false)
            {
                Dictionary<string, string> newLogin = new Dictionary<string, string>()
            {
                { "UserID", HandleID.GenerateID() },
                { "Username", User.Username },
                { "Password", HashPassword.ComputeSha256Hash(User.Password)},
                { "RoleID", SelectedRole.RoleID},
                { "CustomerID", SelectedCustomer.CustomerId }
                //TODO: Generate unique URL
            };

                var statusCode = await APIConnection.Post($"api/users", newLogin);

                if (statusCode == 201)
                {
                    MessageBox.Show("Login oprettet", "Succes");
                    User.Username = "";
                    User.Password = "";
                }
                else
                {
                    MessageBox.Show("Oprettelse af login fejlede. Fejlkode: " + statusCode, "Fejl");
                }
            }
            else
            {
                MessageBox.Show("Brugernavn findes allerede.", "Fejl");
            }
        });

    }
}
