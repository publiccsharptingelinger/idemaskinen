﻿using AdminPanel.Model;
using AdminPanel.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminPanel.ViewModel
{
    class LoginViewModel : Bindable
    {
        private User user = new User();
        public User User { get { return user; } set { user = value; OnPropertyChanged(); } }

        private string spinnerVisible = "hidden";
        public string SpinnerVisible { get { return spinnerVisible; } set { spinnerVisible = value; OnPropertyChanged(); } }

        public ICommand LoginCommand => new Command(async (Object obj) =>
        {
            SpinnerVisible = "visible";
            User loggedInUser = new User();
            var response = await APIConnection.Get($"api/getusernames/{User.Username}");
            loggedInUser = await response.Content.ReadAsAsync<User>();


            if(string.IsNullOrEmpty(User.Username) || string.IsNullOrEmpty(User.Password))
            {
                Console.WriteLine("LOGIN FAILED");
                SpinnerVisible = "hidden";
                MessageBox.Show("Indtast brugernavn og adgangskode.", "Login fejlede. ");
            } else if (loggedInUser == null)
            {
                Console.WriteLine("LOGIN FAILED");
                MessageBox.Show("Brugernavn eller adgangskode forkert.", "Login fejlede. ");
                user.Password = "";
                user.Username = "";
                SpinnerVisible = "hidden";

            } else if(loggedInUser.Username.ToLower().Equals(User.Username.ToLower()))
            {
                if (HashPassword.ComputeSha256Hash(User.Password).Equals(loggedInUser.Password))
                {
                    response = await APIConnection.Get($"api/getrolenames/{loggedInUser.RoleId}");
                    string rolename = await response.Content.ReadAsAsync<string>();

                    if (rolename.ToLower().Equals("admin"))
                    {
                        Console.WriteLine("LOGIN SUCCESSS");
                        MainwindowView mainWindow = new MainwindowView();
                        Application.Current.Windows[0].Close();
                        mainWindow.Show();
                        user.Password = "";
                        user.Username = "";
                        SpinnerVisible = "hidden";
                    } else
                    {
                        Console.WriteLine("LOGIN FAILED");
                        MessageBox.Show("Du har ikke adgang til denne side.", "Login fejlede");
                        user.Password = "";
                        user.Username = "";
                        SpinnerVisible = "hidden";
                    }
                }
                else
                {
                    Console.WriteLine("LOGIN FAILED");
                    MessageBox.Show("Brugernavn eller adgangskode forkert.", "Login fejlede. ");
                    SpinnerVisible = "hidden";
                }
            }

        });
    }
}
