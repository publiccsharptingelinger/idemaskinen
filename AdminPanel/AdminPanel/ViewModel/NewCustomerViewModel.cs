﻿using AdminPanel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminPanel.ViewModel
{
    class NewCustomerViewModel : Bindable
    {
        private Customer customer = new Customer();
        public Customer Customer { get { return customer; } set { customer = value; OnPropertyChanged(); } }

        public ICommand NewCustomerCommand => new Command(async (Object obj) =>
        {

            var customerId = HandleID.GenerateID();

            var url = $"https://idemaskinen.z16.web.core.windows.net/index.html?customerId={customerId}"; 

            Dictionary<string, string> newCustomer = new Dictionary<string, string>()
            {
                { "CustomerID", customerId },
                { "Name", Customer.Name },
                { "Email", Customer.Email },
                { "UniqueURL", url }
            };

            var statusCode = await APIConnection.Post("api/Customers/", newCustomer);

            if(statusCode == 201)
            {
                Clipboard.SetText(url);
                MessageBox.Show("Kunde oprettet\nKundens URL er kopieret til udklipsholderen", "Succes");
                Customer.Name = "";
                Customer.Email = "";
                url = "";
            } else
            {
                MessageBox.Show("Oprettelse af kunde fejlede. Fejlkode: " + statusCode, "Fejl");
            }
        });

    }
}
