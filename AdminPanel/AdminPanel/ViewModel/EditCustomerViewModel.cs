﻿using AdminPanel.Model;
using AdminPanel.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;



namespace AdminPanel.ViewModel
{
    class EditCustomerViewModel : Bindable
    {

        private ObservableCollection<Customer> customerList;
        public ObservableCollection<Customer> CustomerList { get { return customerList; } }

        private Customer selectedCustomer = new Customer();
        public Customer SelectedCustomer { get { return selectedCustomer; } set { selectedCustomer = value; OnPropertyChanged(); } }

        public EditCustomerViewModel()
        {
            Console.WriteLine("TAB ÅBNET");
            customerList = new ObservableCollection<Customer>();
            GetCustomersAsync();
        }

        //Get customers
        private async Task GetCustomersAsync()
        {
            var response = await APIConnection.Get("api/customers/");
            var customers = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            foreach (var customer in customers)
            {
                customerList.Add(customer);
            }
        }

        //Edit customers
        public ICommand EditCustomerCommand => new Command(async (object obj) =>
        {
            var result = MessageBox.Show("Opdater denne kunde?", "Advarsel", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                var statusCode = await APIConnection.Put($"api/Customers/{SelectedCustomer.CustomerId}", SelectedCustomer);
                if (statusCode == 200 || statusCode == 204)
                {
                    MessageBox.Show("Kunde opdateret", "Succes");
                    SelectedCustomer.CustomerId = "";
                    SelectedCustomer.Name = "";
                    SelectedCustomer.Email = "";
                    SelectedCustomer.UniqueURL = "";
                }
                else
                {
                    Console.WriteLine("FEJLKODE: " + statusCode);
                    MessageBox.Show("Der opstod en fejl. Prøv igen", "Fejl");
                }
            }
        });

    }
}
