﻿using AdminPanel.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminPanel.ViewModel
{
    class DeleteCustomerViewModel : Bindable
    {

        private ObservableCollection<Customer> customerList;
        public ObservableCollection<Customer> CustomerList { get { return customerList; } }

        private Customer selectedCustomer = new Customer();
        public Customer SelectedCustomer { get { return selectedCustomer; } set { selectedCustomer = value; OnPropertyChanged(); } }

        public DeleteCustomerViewModel()
        {
            Console.WriteLine("TAB ÅBNET");
            customerList = new ObservableCollection<Customer>();
            GetCustomersAsync();
        }

        private async Task GetCustomersAsync()
        {
            var response = await APIConnection.Get("api/customers/");
            var customers = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            foreach (var customer in customers)
            {
                customerList.Add(customer);
            }
        }

        public ICommand DeleteCustomerCommand => new Command(async (object obj) =>
        {
            var response = await APIConnection.Delete($"api/customers/{selectedCustomer.CustomerId}");

            if(response == 200 || response == 204)
            {
                customerList.Remove(SelectedCustomer);
                MessageBox.Show("Kunde slettet", "Succes");
            } else
            {
                MessageBox.Show("Der opstod en fejl. Prøv igen", "Fejl");
            }


        });

    }
}
