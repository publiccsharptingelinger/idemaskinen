﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Model
{
    public class Customer : Bindable
    {

        private string customerId;
        public string CustomerId { get { return customerId; } set { customerId = value; OnPropertyChanged(); } }

        private string name;
        public string Name { get { return name; } set { name = value; OnPropertyChanged(); } }

        private string email;
        public string Email { get { return email; } set { email = value; OnPropertyChanged(); } }

        private string uniqueURL;
        public string UniqueURL { get { return uniqueURL; } set { uniqueURL = value; OnPropertyChanged(); } }

    }
}
