﻿using AdminPanel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Model
{
    class User : Bindable
    {
        private string userId;
        public string UserId { get { return userId; } set { userId = value; OnPropertyChanged(); } }

        private string username;
        public string Username { get { return username; } set { username = value; OnPropertyChanged(); } }

        private string password;
        public string Password { get { return password; } set { password = value; OnPropertyChanged(); } }

        private string roleId;
        public string RoleId { get { return roleId; } set { roleId = value; OnPropertyChanged(); } }

        private string customerId;
        public string CustomerId { get { return customerId; } set { customerId = value; OnPropertyChanged(); } }


    }
}
