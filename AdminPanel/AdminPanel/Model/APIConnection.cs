﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Model
{
    public static class APIConnection
    {

        public static async Task<HttpResponseMessage> Get(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://linakwebapi.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return response;
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                    return response;
                }
            }
        }

        public static async Task<int> Post(string url, Dictionary<string, string> values)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://linakwebapi.azurewebsites.net/");
                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync(url, content);

                if(response.IsSuccessStatusCode)
                {
                    return (int)response.StatusCode;
                } else
                {
                    return (int)response.StatusCode;
                }

                
            }
        }

        public static async Task<int> Delete(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://linakwebapi.azurewebsites.net/");

                var response = await client.DeleteAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    return (int)response.StatusCode;
                }
                else
                {
                    return (int)response.StatusCode;
                }
            }
        }

        public static async Task<int> Put(string url, object customer)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://linakwebapi.azurewebsites.net/");

                var response = await client.PutAsJsonAsync(url, customer);

                if (response.IsSuccessStatusCode)
                {
                    return (int)response.StatusCode;
                }
                else
                {
                    return (int)response.StatusCode;
                }
            }
        }
    }
}
