﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Model
{
    public class HandleID
    {
        static Random rand = new Random();
        
        public static string GenerateID()
        {
            string number = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);

            number += rand.Next(10000000, 99999999);

            return number;
        }
    }
}
