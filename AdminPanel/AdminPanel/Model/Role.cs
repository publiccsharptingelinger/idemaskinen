﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminPanel.Model
{
    public class Role : Bindable
    {

        private string roleId;
        public string RoleID { get { return roleId; } set { roleId = value; OnPropertyChanged(); } }

        private string rolename;
        public string Rolename { get { return rolename; } set { rolename = value; OnPropertyChanged(); } }

    }
}
