﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminPanel.Model;
using Xunit;

namespace AdminPanel.Test
{
    public class HashPasswordTest
    {
        [Fact]
        public void ComputeSha256HashTest()
        {
            string expected = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08";

            string actual = HashPassword.ComputeSha256Hash("test");

            Assert.Equal(expected, actual);
        }

    }
}
