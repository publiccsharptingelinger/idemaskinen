﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AdminPanel.Model;
using Xunit;

namespace AdminPanel.Test
{
    public class HandleIDTest
    {
        //Test if generated ID is 17 characters long.
        [Fact]
        public void GenerateIDTest()
        {
            int expected = 17;

            int actual = HandleID.GenerateID().Length;
            Assert.Equal(expected, actual);
        }

        //Test if ID is unique
        [Fact]
        public void TestUniqueID()
        {
            int expected = 9999;
            Dictionary<string, int> uniqueIds = new Dictionary<string, int>();
            for (int i = 0; i < 9999; i++)
            {
                string id = HandleID.GenerateID();
                uniqueIds.Add(id, i);
            }
            Assert.Equal(expected, uniqueIds.Count);
        }

    }
}
