﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class ActivitiesController : ApiController
    {
        private LinakProjectDBEntities3 db = new LinakProjectDBEntities3();

        [HttpGet]
        [Route("api/Activities/GetCustomersActivities/{customerId}")]
        public IQueryable<Activities> GetCustomerActivities(string customerId)
        {
            return db.Activities.Where(activity => activity.CustomerID == customerId).OrderByDescending(activity => activity.Date).Take(5);
        }

        // GET: api/Activities
        public IQueryable<Activities> GetActivities()
        {
            return db.Activities;
        }

        // GET: api/Activities/5
        [ResponseType(typeof(Activities))]
        public IHttpActionResult GetActivities(string id)
        {
            Activities activities = db.Activities.Find(id);
            if (activities == null)
            {
                return NotFound();
            }

            return Ok(activities);
        }

        // PUT: api/Activities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutActivities(string id, Activities activities)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != activities.ActivityID)
            {
                return BadRequest();
            }

            db.Entry(activities).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActivitiesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Activities
        [ResponseType(typeof(Activities))]
        public IHttpActionResult PostActivities(Activities activities)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Activities.Add(activities);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ActivitiesExists(activities.ActivityID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = activities.ActivityID }, activities);
        }

        // DELETE: api/Activities/5
        [ResponseType(typeof(Activities))]
        public IHttpActionResult DeleteActivities(string id)
        {
            Activities activities = db.Activities.Find(id);
            if (activities == null)
            {
                return NotFound();
            }

            db.Activities.Remove(activities);
            db.SaveChanges();

            return Ok(activities);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActivitiesExists(string id)
        {
            return db.Activities.Count(e => e.ActivityID == id) > 0;
        }
    }
}