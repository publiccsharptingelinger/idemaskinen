﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    //[EnableCors(origins: "http://linakwebapi.azurewebsites.net", headers: "*", methods: "*")]
    public class IdeasController : ApiController
    {
        public IAPIContext db = new LinakProjectDBEntities3();

        public IdeasController() { }

        public IdeasController(IAPIContext context)
        {
            db = context;
        }

        public string HandleId { get; private set; }

        [Route("api/Ideas/GetCustomersIdeas/{customerId}")]
        [HttpGet]
        public IQueryable<Ideas> GetCustomersIdeas(string customerId)
        {
            return db.Ideas.Where(idea => idea.CustomerID == customerId);
        }

        [Route("api/Ideas/{ideaId:alpha}")]
        [HttpGet]
        public Ideas GetIdeasCustomer(string ideaId)
        {
            return db.Ideas.Where(idea => idea.IdeaID == ideaId).FirstOrDefault();
        }

        // GET: api/Ideas
        public IQueryable<Ideas> GetIdeas()
        {
            return db.Ideas;
        }

        // GET: api/Ideas/5
        [ResponseType(typeof(Ideas))]
        public IHttpActionResult GetIdeas(string id)
        {
            Ideas ideas = db.Ideas.Find(id);
            if (ideas == null)
            {
                return NotFound();
            }

            return Ok(ideas);
        }

        // PUT: api/Ideas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutIdeas(string id, Ideas ideas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ideas.IdeaID)
            {
                return BadRequest();
            }

            //db.Entry(ideas).State = EntityState.Modified;
            db.MarkAsModified(ideas);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IdeasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ideas
        [ResponseType(typeof(Ideas))]
        public IHttpActionResult PostIdeas(Ideas ideas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ideas.Add(ideas);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (IdeasExists(ideas.IdeaID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = ideas.IdeaID }, ideas);
        }

        // DELETE: api/Ideas/5
        [ResponseType(typeof(Ideas))]
        public IHttpActionResult DeleteIdeas(string id)
        {
            Ideas ideas = db.Ideas.Find(id);
            if (ideas == null)
            {
                return NotFound();
            }

            db.Ideas.Remove(ideas);
            db.SaveChanges();

            return Ok(ideas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IdeasExists(string id)
        {
            return db.Ideas.Count(e => e.IdeaID == id) > 0;
        }
    }
}