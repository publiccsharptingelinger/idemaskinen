﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI.WebControls;
using API.Models;

namespace API.Controllers
{
    public class FilesController : ApiController
    {
        private LinakProjectDBEntities3 db = new LinakProjectDBEntities3();

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [HttpPost]
        [Route("api/Files/Upload")]
        public IHttpActionResult UploadFileAsync()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection  
                var httpPostedFile = HttpContext.Current.Request.Files["file"];
                var fileid = HttpContext.Current.Request.Form["FileID"];
                if (httpPostedFile != null)
                {
                    Files file = new Files();
                    int length = httpPostedFile.ContentLength;
                    file.Binary = new byte[length]; //get imagedata  
                    httpPostedFile.InputStream.Read(file.Binary, 0, length);
                    file.Name = Path.GetFileName(httpPostedFile.FileName);
                    file.FileID = fileid;
                    db.Files.Add(file);
                    db.SaveChanges();
                    //var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), httpPostedFile.FileName);
                    // Save the uploaded file to "UploadedFiles" folder  
                    //httpPostedFile.SaveAs(fileSavePath);
                    return Ok("Image Uploaded");
                }
            }
            return Ok("Image is not Uploaded");

            /*if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            string fileid = HttpContext.Current.Request.Form["FileID"];

            foreach (var file in provider.Contents)
            {
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                var buffer = await file.ReadAsByteArrayAsync();
                //Do whatever you want with filename and its binary data.
                db.Files.Add(new Files()
                {
                    FileID = fileid,
                    Name = filename,
                    Binary = buffer
                });
            }

            db.SaveChanges();

            return Ok();*/
        }


        // GET: api/Files
        public IQueryable<Files> GetFiles()
        {
            return db.Files;
        }

        // GET: api/Files/5
        [ResponseType(typeof(Files))]
        public IHttpActionResult GetFiles(string id)
        {
            Files files = db.Files.Find(id);
            if (files == null)
            {
                return NotFound();
            }

            return Ok(files);
        }

        // PUT: api/Files/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFiles(string id, Files files)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != files.FileID)
            {
                return BadRequest();
            }

            db.Entry(files).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FilesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Files
        [ResponseType(typeof(Files))]
        public IHttpActionResult PostFiles(Files files)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Files.Add(files);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FilesExists(files.FileID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = files.FileID }, files);
        }

        // DELETE: api/Files/5
        [ResponseType(typeof(Files))]
        public IHttpActionResult DeleteFiles(string id)
        {
            Files files = db.Files.Find(id);
            if (files == null)
            {
                return NotFound();
            }

            db.Files.Remove(files);
            db.SaveChanges();

            return Ok(files);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FilesExists(string id)
        {
            return db.Files.Count(e => e.FileID == id) > 0;
        }
    }
}