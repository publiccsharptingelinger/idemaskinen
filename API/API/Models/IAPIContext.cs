﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace API.Models
{
    public interface IAPIContext: IDisposable
    {

        DbSet<Ideas> Ideas { get; }
        int SaveChanges();
        void MarkAsModified(Ideas item);
    }
}