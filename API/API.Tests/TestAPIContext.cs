﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Tests
{
    class TestAPIContext : IAPIContext
    {
        public TestAPIContext()
        {
            this.Ideas = new TestIdeaDBSet();
        }

        public DbSet<Ideas> Ideas { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Ideas item) { }
        public void Dispose() { }
    }
}
