﻿using API.Controllers;
using API.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace API.Tests
{
    [TestClass]
    public class TestIdeasController
    {
        [TestMethod]
        public void PostIdea_ShouldReturnSameIdea()
        {
            var controller = new IdeasController(new TestAPIContext());

            var item = GetDemoIdeas();

            var result =
                controller.PostIdeas(item) as CreatedAtRouteNegotiatedContentResult<Ideas>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "DefaultApi");
            Assert.AreEqual(result.RouteValues["id"], result.Content.IdeaID);
            Assert.AreEqual(result.Content.Title, item.Title);
        }

        [TestMethod]
        public void PutIdea_ShouldReturnStatusCode()
        {
            var controller = new IdeasController(new TestAPIContext());

            var item = GetDemoIdeas();

            var result = controller.PutIdeas(item.IdeaID, item) as StatusCodeResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [TestMethod]
        public void PutIDea_ShouldFail_WhenDifferentID()
        {
            var controller = new IdeasController(new TestAPIContext());

            var badresult = controller.PutIdeas("999", GetDemoIdeas());
            Assert.IsInstanceOfType(badresult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetIDea_ShouldReturnIdeaWithSameID()
        {
            var context = new TestAPIContext();
            context.Ideas.Add(GetDemoIdeas());

            var controller = new IdeasController(context);
            var result = controller.GetIdeas("3") as OkNegotiatedContentResult<Ideas>;

            Assert.IsNotNull(result);
            Assert.AreEqual("3", result.Content.IdeaID);
        }

        [TestMethod]
        public void GetIdeas_ShouldReturnAllIdeas()
        {
            var context = new TestAPIContext();
            context.Ideas.Add(new Ideas { IdeaID = "1", Title = "TestIdea1", Description = "Test idea 1" });
            context.Ideas.Add(new Ideas { IdeaID = "2", Title = "TestIdea2", Description = "Test idea 2" });
            context.Ideas.Add(new Ideas { IdeaID = "2", Title = "Demo3", Description = "Test idea 3" });

            var controller = new IdeasController(context);
            var result = controller.GetIdeas() as TestIdeaDBSet;

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Local.Count);
        }

        [TestMethod]
        public void DeleteIdea_ShouldReturnOK()
        {
            var context = new TestAPIContext();
            var item = GetDemoIdeas();
            context.Ideas.Add(item);

            var controller = new IdeasController(context);
            var result = controller.DeleteIdeas("3") as OkNegotiatedContentResult<Ideas>;

            Assert.IsNotNull(result);
            Assert.AreEqual(item.IdeaID, result.Content.IdeaID);
        }

        Ideas GetDemoIdeas()
        {
            return new Ideas() { IdeaID = "3", Title = "Demo idea", Description = "demo idea" };
        }
    }
}
