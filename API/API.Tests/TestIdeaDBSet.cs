﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API;
using API.Models;

namespace API.Tests
{
    class TestIdeaDBSet : TestDbSet<Ideas>
    {
        public override Ideas Find(params object[] keyValues)
        {
            return this.SingleOrDefault(idea => idea.IdeaID == (string)keyValues.Single());
        }
    }
}
