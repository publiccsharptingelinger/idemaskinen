const logoutBtn = document.querySelector('#logoutBtn');
let user = JSON.parse(sessionStorage.getItem('user'));

document.addEventListener("DOMContentLoaded", () => {
  let user = JSON.parse(sessionStorage.getItem('user'));
  const welcomeLink = document.querySelector('#welcomeLink');
  welcomeLink.innerText += user.Username;

  getActivities();
  setInterval(() => {
    getActivities();
  }, 10000);
})

logoutBtn.addEventListener('click', (e) => {
  sessionStorage.removeItem('user');
  sessionStorage.removeItem('authenticated');
  window.location.replace("../index.html");
})


const actitvityTemplate = (activity, idea) => {
  const activityList = document.querySelector('#activityList');
  let color;

  if (idea.StatusID === 1) {
    color = 'color_new';
  } else if (idea.StatusID === 2 || idea.StatusID === 3) {
    color = 'color_ongoing';
  } else if (idea.StatusID === 4) {
    color = 'color_implement';
  }

  activityList.innerHTML += `
    <li class="${color}">
        <div class="activity-content-container">
            <div class="info-col">
                <span class="timestamp">${activity.Author} | ${timeSince(activity.Date.replace('T', ' '))} siden<br></span>
                <span class="action">${activity.Activity}<br></span>
                <a href="#"><span class="action-detail">${idea.Title}</span></a>
            </div>
        </div>
    </li>
    `;
}

const getActivities = () => {
  let counter = 0;
  let counter2 = 0;

  $.ajax({
    type: "GET",
    url: `https://linakwebapi.azurewebsites.net/api/activities/getcustomersactivities/${user.CustomerID}`,
    async: false
  }).done((data) => {
    document.querySelector('#activityList').innerHTML = '';

    data.forEach(activity => {
      $.ajax({
        type: "GET",
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${activity.IdeaID}`,
        async: false
      }).done((ideaData) => {
        actitvityTemplate(activity, ideaData)
      })
    });
  });
}

var timeSince = function (date) {
  if (typeof date !== 'object') {
    date = new Date(date);
  }

  var seconds = Math.floor((new Date() - date) / 1000);
  var intervalType;

  var interval = Math.floor(seconds / 31536000);
  if (interval >= 1) {
    intervalType = 'år';
  } else {
    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
      intervalType = 'måneder';
    } else {
      interval = Math.floor(seconds / 86400);
      if (interval >= 1) {
        intervalType = 'dage';
      } else {
        interval = Math.floor(seconds / 3600);
        if (interval >= 1) {
          intervalType = "timer";
        } else {
          interval = Math.floor(seconds / 60);
          if (interval >= 1) {
            intervalType = "minutter";
          } else {
            interval = seconds;
            intervalType = "sekunder";
          }
        }
      }
    }
  }

  if (interval > 1 || interval === 0) {
    intervalType += 's';
  }

  return interval + ' ' + intervalType;
};

