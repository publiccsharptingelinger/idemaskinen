var queryString = decodeURIComponent(window.location.search);
var ideaId = queryString.split('=')[1].split('&')[0];
let user = JSON.parse(sessionStorage.getItem('user'));

let ideaData;

$.get(`https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`, (data) => {
    ideaData = data;
    viewIdeaInfo(data);
    getComments();
    userTemplate(data);
});

const acceptIdeaBtn = document.querySelector('#acceptIdeaBtn');
const declineIdeaBtn = document.querySelector('#declineIdeaBtn');
const prioritySlider = document.querySelector('#priorityInputId');
const effortSlider = document.querySelector('#indsatsInputId');
const impactSlider = document.querySelector('#IndvirkInputId');
const savingsBox = document.querySelector('#Besparelser');
const submitCommentBtn = document.querySelector('#submitPost');
const resultsBtn = document.querySelector('#resultsBtn');
const challengesBtn = document.querySelector('#challengesBtn');
const addUserBtn = document.querySelector('#addUserBtn');



document.addEventListener('DOMContentLoaded', () => {


    setInterval(() => {
        getComments();
    }, 5000);

});

acceptIdeaBtn.addEventListener('click', (e) => {
    e.preventDefault();
    ideaData.StatusID += 1;
    console.log(ideaData.StatusID);
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            console.log(ideaData.StatusID);
            postActivity(ideaId, 'opdaterede idé status', user.Username, user.CustomerID);
            alert('Idé status opdateret');
            window.location.replace("index.html");
        }
    });
})

declineIdeaBtn.addEventListener('click', (e) => {
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'DELETE',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'afviste en idé', user.Username, user.CustomerID);
            alert('Idé afvist');
            window.location.replace("index.html");
        }
    });
})



submitCommentBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const comment = document.querySelector('#postMessage');
    let date = (new Date()).toLocaleString("en-US");
    console.log(comment.value);

    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/comments`,
        type: 'POST',
        data: {
            CommentID: generateId(),
            IdeaID: ideaId,
            Comment: comment.value,
            Date: date,
            Author: user.Username
        },
        success: (data) => {
            postActivity(ideaId, 'kommenterede en idé', user.Username, user.CustomerID);
            getComments();
        }
    });

})

resultsBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const results = document.querySelector('#Resultater');

    ideaData.Results = results.value;
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'opdaterede resultater', user.Username, user.CustomerID);
            alert('Resultater opdateret');
        }
    });
})

challengesBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const challenges = document.querySelector('#Udfordringer');

    ideaData.Challenges = challenges.value;

    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'opdaterede udfordringer', user.Username, user.CustomerID);
            alert('Udfordringer opdateret');
        }
    });
})

addUserBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const username = document.querySelector('#addUserInput').value;

    ideaData.AddedUsers += username + ';';
    console.log(ideaData.AddedUsers);
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'tilføjede en bruger', user.Username, user.CustomerID);
            alert('Bruger tilføjet');
        }
    });
})

prioritySlider.addEventListener('change', (e) => {
    console.log(e.target.value)
    ideaData.Priority = e.target.value;
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'redigerede prioritet', user.Username, user.CustomerID);
            alert('Prioritet ændret');
        }
    });
})

effortSlider.addEventListener('change', (e) => {
    console.log(e.target.value)
    ideaData.Effort = e.target.value;
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'redigerede indsats', user.Username, user.CustomerID);
            alert('Indsats ændret');
        }
    });
})

impactSlider.addEventListener('change', (e) => {
    console.log(e.target.value)
    ideaData.Impact = e.target.value;
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'redigerede indvirkning', user.Username, user.CustomerID);
            alert('Indvirkning ændret');
        }
    });
})

savingsBox.addEventListener('change', (e) => {
    console.log(e.target.value)
    ideaData.CostSavings = e.target.value;
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(ideaData),
        success: (data) => {
            postActivity(ideaId, 'redigerede besparelser', user.Username, user.CustomerID);
            alert('Besparelse ændret');

        }
    });

})

const viewIdeaInfo = (ideaInfo) => {
    const statusUnderImplementation = document.querySelector('#statusUnderImplementation');
    const statusImplemented = document.querySelector('#statusImplemented');

    const ideaTitle = document.querySelector('#ideaTitle');
    const ideaAuthor = document.querySelector('#ideaAuthor');
    const ideaDate = document.querySelector('#ideaDate');
    const ideaDesc = document.querySelector('#ideaDescription');
    const priorityOutputId = document.querySelector('#priorityOutputId');
    const priorityInputId = document.querySelector('#priorityInputId');
    const effortOutputId = document.querySelector('#indsatsOutputId');
    const effortInputId = document.querySelector('#indsatsInputId');
    const impactOutputId = document.querySelector('#IndvirkOutputId');
    const impactInputId = document.querySelector('#IndvirkInputId');
    const savings = document.querySelector('#Besparelser');
    const challenges = document.querySelector('#Udfordringer');
    const results = document.querySelector('#Resultater');
    const hashtagList = document.querySelector('#hashtagList');


    if (ideaInfo.StatusID === 3) {
        statusUnderImplementation.setAttribute("class", "active");
    } else if (ideaInfo.StatusID === 4) {
        statusUnderImplementation.setAttribute("class", "active");
        statusImplemented.setAttribute("class", "active");
    }

    ideaTitle.innerText = ideaInfo.Title;
    ideaAuthor.innerText += " " + ideaInfo.EmployeeID;
    ideaDate.innerText += " " + ideaInfo.CreationDate.replace('T', ' ');
    ideaDesc.innerText = "" + ideaInfo.Description;
    hashtagList.innerText = ideaInfo.Tags


    if (ideaInfo.Priority === null) {
        priorityOutputId.innerText = 1;
        priorityInputId.value = 1;
    } else {
        priorityOutputId.innerText = ideaInfo.Priority;
        priorityInputId.value = ideaInfo.Priority;
    }

    effortOutputId.innerText = ideaInfo.Effort;
    effortInputId.value = ideaInfo.Effort;

    impactOutputId.innerText = ideaInfo.Impact;
    impactInputId.value = ideaInfo.Impact;

    if (ideaInfo.CostSavings !== null) {
        savings.value = ideaInfo.CostSavings;
    }

    if (ideaInfo.challenges === null) {
        challenges.value = '';
    } else {
        challenges.value = ideaInfo.Challenges;
    }

    if (ideaInfo.Results === null) {
        results.value = '';
    } else {
        results.value = ideaInfo.Results;
    }

    
}

const commentTemplate = (comments) => {
    const commentList = document.querySelector('.comment-list');
    commentList.innerHTML = "";
    comments.forEach(comment => {
        commentList.innerHTML += `
        <div class="post">
            <div class="col-sm-10">
                <p class="post-date">Kommentar den <time datetime="${comment.Date.replace('T', ' ')}">${comment.Date.replace('T', ' ')}</time> af <a class="post-author" href="#">${comment.Author}</a></p>
                <div><p>${comment.Comment}</p></div>
            </div>
        </div>
        `
    });
}

const getComments = () => {

    $.get(`https://linakwebapi.azurewebsites.net/api/comments/getideascomments/${ideaId}`, (data) => {
        commentTemplate(data);
    });
}


const userTemplate = (ideaInfo) => {
    const userList = document.querySelector('#userList');
    console.log(ideaInfo.AddedUsers);

    if(ideaInfo.AddedUsers === null) {
        userList.innerHTML = '';
    } else {
        const users = ideaInfo.AddedUsers.split(';');
        users.forEach(user => {
            userList.innerHTML += `
            <div>
                <div class="icon-col">
                    <p>${user}</p>
                </div>
            </div>
            `
        });
    }

    
}