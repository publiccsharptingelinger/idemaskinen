(function() {
  $(function() {
    return $("#postMessage").keydown(function() {
      var wordCount;
      wordCount = 3000 - $("#postMessage").val().length;
      if (wordCount < 1) {
        $("#postMessage").val($("#postMessage").val().substring(0, 139));
        return $("#wordCounter").html(0);
      } else {
        return $("#wordCounter").html(wordCount);
      }
    });
  });

}).call(this);