var grid = null;
var docElem = document.documentElement;
var demo = document.querySelector('.grid-demo');
var gridElement = demo.querySelector('.grid');
var filterField = demo.querySelector('.filter-field');
var searchField = demo.querySelector('.search-field');
var dragOrder = [];
var filterFieldValue;
var sortFieldValue;
var searchFieldValue;


document.addEventListener('DOMContentLoaded', function () {

  let user = JSON.parse(sessionStorage.getItem('user'));
  const welcomeLink = document.querySelector('#welcomeLink');
  welcomeLink.innerText += user.Username;

  function startGrid() {

    initGrid();
    searchField.value = '';
    [filterField].forEach(function (field) {
      field.value = field.querySelectorAll('option')[0].value;
    });

    searchFieldValue = searchField.value.toLowerCase();
    filterFieldValue = filterField.value;

    searchField.addEventListener('keyup', function () {
      var newSearch = searchField.value.toLowerCase();
      if (searchFieldValue !== newSearch) {
        searchFieldValue = newSearch;
        filter();
      }
    });

    filterField.addEventListener('change', filter);
    gridElement.addEventListener('click', function (e) {
      if (elementMatches(e.target, '.card-remove, .card-remove i')) {
        removeItem(e);
      }
    });

  }

  function initGrid() {

    var dragCounter = 0;

    grid = new Muuri(gridElement, {
      layoutDuration: 400,
      layoutEasing: 'ease',
      dragEnabled: false,
      dragSortInterval: 50,
      dragContainer: document.body,
      dragStartPredicate: function (item, event) {
        var isRemoveAction = elementMatches(event.target, '.card-remove, .card-remove i');
        return isDraggable && !isRemoveAction ? Muuri.ItemDrag.defaultStartPredicate(item, event) : false;
      },
      dragReleaseDuration: 400,
      dragReleseEasing: 'ease'
    })
      .on('dragStart', function () {
        ++dragCounter;
        docElem.classList.add('dragging');
      })
      .on('dragEnd', function () {
        if (--dragCounter < 1) {
          docElem.classList.remove('dragging');
        }
      })
      .on('move', updateIndices)
      .on('sort', updateIndices);

  }

  function filter() {

    filterFieldValue = filterField.value;
    grid.filter(function (item) {
      var element = item.getElement();
      var isSearchMatch = !searchFieldValue ? true : (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
      var isFilterMatch = !filterFieldValue ? true : (element.getAttribute('data-color') || '') === filterFieldValue;
      return isSearchMatch && isFilterMatch;
    });

  }

  function sort() {

    var currentSort = sortField.value;
    if (sortFieldValue === currentSort) {
      return;
    }

    if (sortFieldValue === 'order') {
      dragOrder = grid.getItems();
    }

    grid.sort(
      currentSort === 'title' ? compareItemTitle :
        currentSort === 'color' ? compareItemColor :
          dragOrder
    );

    updateIndices();
    sortFieldValue = currentSort;

  }

  function changeLayout() {

    layoutFieldValue = layoutField.value;
    grid._settings.layout = {
      horizontal: false,
      alignRight: layoutFieldValue.indexOf('right') > -1,
      alignBottom: layoutFieldValue.indexOf('bottom') > -1,
      fillGaps: layoutFieldValue.indexOf('fillgaps') > -1
    };
    grid.layout();

  }

  function compareItemTitle(a, b) {

    var aVal = a.getElement().getAttribute('data-title') || '';
    var bVal = b.getElement().getAttribute('data-title') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : 0;

  }

  function compareItemColor(a, b) {

    var aVal = a.getElement().getAttribute('data-color') || '';
    var bVal = b.getElement().getAttribute('data-color') || '';
    return aVal < bVal ? -1 : aVal > bVal ? 1 : compareItemTitle(a, b);

  }

  function updateIndices() {

    grid.getItems().forEach(function (item, i) {
      item.getElement().setAttribute('data-id', i + 1);
      item.getElement().querySelector('.card-id').innerHTML = i + 1;
    });

  }

  function elementMatches(element, selector) {

    var p = Element.prototype;
    return (p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || p.oMatchesSelector).call(element, selector);

  }
  populateIdeas();
  startGrid();

});

//Generate DOM node of an idea
const ideaTemplate = (color, idea) => {

  const root = document.createElement('div');
  root.setAttribute("class", `item h5 w2 ${color}`);
  root.setAttribute("data-id", `${idea.IdeaID}`);
  root.setAttribute("data-color", `${color}`);
  root.setAttribute("data-title", `${idea.Title}`);

  const a = document.createElement('a');
  a.setAttribute("href", `idea.html?id=${idea.IdeaID}&statusid=${idea.StatusID}`);
  root.appendChild(a);

  const container = document.createElement('div');
  container.setAttribute("class", "item-content");
  a.appendChild(container);

  const card = document.createElement('div');
  card.setAttribute("class", "card");
  container.appendChild(card);

  const id = document.createElement('div');
  id.setAttribute("class", "card-id");
  id.innerText = `${idea.IdeaID}`;
  card.appendChild(id);

  const title = document.createElement('div');
  title.setAttribute("class", "card-title");
  title.innerText = `${idea.Title}`;
  card.appendChild(title);

  return root;
}

//Fetch ideas and populate the grid
const populateIdeas = () => {

  const customerId = localStorage.getItem('customerId');
  fetch(`https://linakwebapi.azurewebsites.net/api/ideas/getcustomersideas/${customerId}`)
    .then(response => response.json())
    .then(data => {
      const ideasArray = [];
      data.forEach((idea) => {
        if (idea.StatusID === 2) {
          ideasArray.push(ideaTemplate("red", idea));
        }
      })
      grid.add(ideasArray);
    });
}
