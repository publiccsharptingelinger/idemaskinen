document.addEventListener('DOMContentLoaded', function () {
  var ideboard = document.querySelector('.ideboard-interface');
  var board = ideboard.querySelector('.board');
  var boardGrid;
  boardGrid = new Muuri(board);
  populateIdeas();
});

//Template for generating new idea
const IdeaTemplate = (htmlElement, idea) => {
  htmlElement.innerHTML += `
    <div class="board-column-content">
    <!-- EKSEMPEL PÅ ET ELEMENT SOM DYNAMISK SKAL TILFØJES -->
    <div class="board-item">
      <a href="idea.html?id=${idea.IdeaID}">
        <div class="board-item-content"><span>${idea.Title}</span></div>
      </a>
    </div>
    <!-- SLUT PÅ EKSEMPEL -->
  </div>`
}

//Fetch ideas and populate the idealists
const populateIdeas = () => {

  const customerId = localStorage.getItem('customerId');
  
  fetch(`https://linakwebapi.azurewebsites.net/api/ideas/getcustomersideas/${customerId}`)
    .then(response => response.json())
    .then(data => {
      data.forEach((idea) => {
        if(idea.StatusID === 1) {
          IdeaTemplate(document.querySelector("#newIdeasList"), idea);
        } else if(idea.StatusID === 3) {
          IdeaTemplate(document.querySelector("#pendingIdeasList"), idea);
        } else if(idea.StatusID === 4) {
          IdeaTemplate(document.querySelector("#implementedIdeasList"), idea);
        }
      })
    });
}