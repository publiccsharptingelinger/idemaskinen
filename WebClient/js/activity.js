const postActivity = (ideaId, activity, author, customerId) => {

    let date = (new Date()).toLocaleString("en-US");

    console.log("aktivitet");
    $.ajax({
        url: `https://linakwebapi.azurewebsites.net/api/activities`,
        type: 'POST',
        data: {
            ActivityID: generateId(),
            IdeaID: ideaId,
            Activity: activity,
            Date: date,
            Author: author,
            customerId: customerId
        },
        success: (data) => {
            console.log("success");
        }
    });
}
