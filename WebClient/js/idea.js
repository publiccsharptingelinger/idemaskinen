var queryString = decodeURIComponent(window.location.search);
var ideaId = queryString.split('=')[1];

let ideaData;

$.get(`https://linakwebapi.azurewebsites.net/api/ideas/${ideaId}`, (data) => {
    ideaData = data;
    viewIdeaInfo(data);
});

document.addEventListener('DOMContentLoaded', () => {

});

const viewIdeaInfo = (ideaInfo) => {
    const statusUnderImplementation = document.querySelector('#statusUnderImplementation');
    const statusImplemented = document.querySelector('#statusImplemented');

    const ideaTitle = document.querySelector('#ideaTitle');
    const ideaAuthor = document.querySelector('#ideaAuthor');
    const ideaDate = document.querySelector('#ideaDate');
    const ideaDesc = document.querySelector('#ideaDescription');
    const effortOutputId = document.querySelector('#indsatsOutputId');
    const effortInputId = document.querySelector('#indsatsInputId');
    const impactOutputId = document.querySelector('#IndvirkOutputId');
    const impactInputId = document.querySelector('#IndvirkInputId');
    const hashtagList = document.querySelector('#hashtagList');



    if (ideaInfo.StatusID === 3) {
        statusUnderImplementation.setAttribute("class", "active");
    } else if (ideaInfo.StatusID === 4) {
        statusUnderImplementation.setAttribute("class", "active");
        statusImplemented.setAttribute("class", "active");
    }

    ideaTitle.innerText = ideaInfo.Title;
    ideaAuthor.innerText += " " + ideaInfo.EmployeeID;
    ideaDate.innerText += " " + ideaInfo.CreationDate.replace('T', ' ');
    ideaDesc.innerText = "" + ideaInfo.Description;

    effortOutputId.innerText = ideaInfo.Effort;
    effortInputId.value = ideaInfo.Effort;

    impactOutputId.innerText = ideaInfo.Impact;
    impactInputId.value = ideaInfo.Impact;

    hashtagList.innerText = ideaInfo.Tags
}