const sliderProps = {
	fill: "#000000",
	background: "rgba(255, 255, 255, 0.214)",
};

const slider1 = document.querySelector(".range__slider1");
const sliderValue1 = document.querySelector(".length__title1");

const slider2 = document.querySelector(".range__slider2");
const sliderValue2 = document.querySelector(".length__title2");


slider1.querySelector("input").addEventListener("input", event => {
	sliderValue1.setAttribute("data-length", event.target.value);
	applyFill1(event.target);
});

applyFill1(slider1.querySelector("input"));

function applyFill1(slider1) {
	const percentage = (100 * (slider1.value - slider1.min)) / (slider1.max - slider1.min);
	const bg = `linear-gradient(90deg, ${sliderProps.fill} ${percentage}%, ${sliderProps.background} ${percentage +
			0.1}%)`;
	slider1.style.background = bg;
	sliderValue1.setAttribute("data-length", slider1.value);
}



slider2.querySelector("input").addEventListener("input", event => {
	sliderValue2.setAttribute("data-length", event.target.value);
	applyFill2(event.target);
});

applyFill2(slider2.querySelector("input"));

function applyFill2(slider2) {
	const percentage = (100 * (slider2.value - slider2.min)) / (slider2.max - slider2.min);
	const bg = `linear-gradient(90deg, ${sliderProps.fill} ${percentage}%, ${sliderProps.background} ${percentage +
			0.1}%)`;
	slider2.style.background = bg;
	sliderValue2.setAttribute("data-length", slider2.value);
}