const submitBtn = document.querySelector('#submitButton');
const ideaForm = document.querySelector('#newIdeaForm');
const tagWrapper = document.querySelector('.tag-wrapper');

$('.contact-form').find('.form-control').each(function () {
  var targetItem = $(this).parent();
  if ($(this).val()) {
    $(targetItem).find('label').css({
      'top': '10px',
      'fontSize': '14px'
    });
  }
})
$('.contact-form').find('.form-control').focus(function () {
  $(this).parent('.input-block').addClass('focus');
  $(this).parent().find('label').animate({
    'top': '10px',
    'fontSize': '14px'
  }, 300);
})
$('.contact-form').find('.form-control').blur(function () {
  if ($(this).val().length == 0) {
    $(this).parent('.input-block').removeClass('focus');
    $(this).parent().find('label').animate({
      'top': '25px',
      'fontSize': '18px'
    }, 300);
  }
})
const getTags = () => {
  let tags = '';
  const tagContainer = document.querySelector('.tag-container');
  tagContainer.childNodes.forEach((tag) => {
    tags += tag.textContent;
  })
  return tags;
}

const postIdea = (fileId) => {
  const empId = document.querySelector("#empId");
  const title = document.querySelector("#title");
  const description = document.querySelector("#description");
  const impactSlider = document.querySelector(".impact-slider");
  const effortSlider = document.querySelector(".effort-slider");
  const date = (new Date()).toLocaleString("en-US");

  const customerId = localStorage.getItem('customerId');

  return $.ajax({
    type: "POST",
    url: `https://linakwebapi.azurewebsites.net/api/ideas`,
    data: {
      IdeaID: generateId(),
      EmployeeID: empId.value,
      Title: title.value,
      Description: description.value,
      Effort: effortSlider.value,
      Impact: impactSlider.value,
      FileID: fileId,
      Tags: getTags(),
      StatusID: 1,
      CustomerID: customerId,
      CreationDate: date,
      Priority: 1
    }
  }).done(() => {
    alert('Idé oprettet')
    const form = document.querySelector("#newIdeaForm");
    form.reset();
  });
}

const postFile = (fileid) => {
  var data = new FormData()
  var file = document.querySelector("#fileUpload").files[0];
  console.log(file);
  // Add the uploaded image content to the form data collection  
  data.append("file", file);
  data.append("FileID", fileid);
  // Make Ajax request with the contentType = false, and procesDate = false  
  return $.ajax({
    type: "POST",
    url: `https://linakwebapi.azurewebsites.net/api/files/upload`,
    contentType: false,
    processData: false,
    data: data
  }).done(() => {
    postIdea(fileid);
  });
}


ideaForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  var file = document.querySelector("#fileUpload").files[0];

  if (file) {
    const fileId = generateId();
    postFile(fileId);
  } else {
    postIdea(null);
  }

});

/*
// With JQuery
$('#ex1').slider({
  formatter: function (value) {
    return 'Current value: ' + value;
  }
});

// Without JQuery
var slider = new Slider('#ex1', {
  formatter: function (value) {
    console.log(value);
    return 'Current value: ' + value;
  }
});*/

