const generateId = () => {
    const date = new Date();
    const ticks = Math.floor((date.getTime() / 10) % 1000000000).toString();
    const randomNumber = Math.floor((Math.random() * 99999999) + 10000000).toString();

    return ticks + randomNumber;
}